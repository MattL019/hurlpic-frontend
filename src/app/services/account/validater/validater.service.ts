import { Injectable } from '@angular/core';
import { AbstractControl, ValidatorFn, FormControl, ValidationErrors, AsyncValidatorFn } from '@angular/forms';
import { Observable } from 'rxjs';
import { SocketService } from '../../socket/socket.service';

@Injectable()
export class ValidaterService {

  constructor() { }

  // Check for unique username
  uniqueUsername(ss: SocketService): AsyncValidatorFn {
    return (control: AbstractControl): Promise<ValidationErrors> => {
      ss.socket.emit('unique-username-ping', control.value);
      return new Promise((resolve, reject) => {
        ss.socket.on('unique-username-pong', (status) => {
          if(!status) resolve(null);
          if(status) resolve({unique: "true"});
        });
      });
    }
  }

  // Check for unique email
  uniqueEmail(ss: SocketService): AsyncValidatorFn {
    return (control: AbstractControl): Promise<ValidationErrors> => {
      ss.socket.emit('unique-email-ping', control.value);
      return new Promise((resolve, reject) => {
        ss.socket.on('unique-email-pong', (status) => {
          if(!status) resolve(null);
          if(status) resolve({unique: "true"});
        });
      });
    }
  }

  // Check for Whitespace
  whitespace(control: AbstractControl): {[key: string]: boolean} | null {
    if(control.value && control.value.indexOf(' ') >= 0) {
      return { "whitespace": true };
    }
    return null;
  }

  // Check for special characters
  specialchars(control: AbstractControl): {[key: string]: boolean} | null {
    let regex = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

    if(regex.test(control.value)) {
      return {"specialchars": true}
    }
    return null;
  }

  match(control: AbstractControl): { [key: string]: boolean } | any {
    if(control.value === control.root.value['password']) {
      return null;
    }
    return { 'nomatch': true }
  }

}
