import { TestBed, inject } from '@angular/core/testing';

import { ValidaterService } from './validater.service';

describe('ValidaterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ValidaterService]
    });
  });

  it('should be created', inject([ValidaterService], (service: ValidaterService) => {
    expect(service).toBeTruthy();
  }));
});
