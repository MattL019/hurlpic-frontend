import { Injectable } from '@angular/core';
import { SocketService } from '../../socket/socket.service';
import { Observable } from 'rxjs';

@Injectable()
export class UserService {
  constructor(private ss: SocketService) { }

  register(credentials: Object): Observable<any> {
    this.ss.socket.emit('register-submit', credentials);
    // Listen for 'register-response'
    let responseObs = Observable.create((obs) => {
      this.ss.socket.on('register-response', (status: boolean) => {
        obs.next(status);
      });
    });
    return responseObs;
  }

  login(credentials: Object): Observable<any> {
    this.ss.socket.emit('login-submit', credentials);
    // Listen for 'login-response'
    let responseObs = Observable.create((obs) => {
      this.ss.socket.on('login-response', (status: boolean) => {
        obs.next(status);
      });
    });
    return responseObs;
  }
}
