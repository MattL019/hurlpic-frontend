import { Injectable } from '@angular/core';
import * as socketio from 'socket.io-client';
import { environment } from '../../../environments/environment';

@Injectable()
export class SocketService {
  socket: any;

  constructor() { 
    console.log("Connecting..." + environment.server_url);
    this.socket = socketio(environment.server_url);
  }
}
