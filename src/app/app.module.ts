import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SocketService } from './services/socket/socket.service';
import { UserService } from './services/account/user/user.service';

import { AppComponent } from './app.component';
import { RegisterComponent } from './core/account/register/register.component';
import { LoginComponent } from './core/account/login/login.component';
import { LogregwrapperComponent } from './core/account/logregwrapper/logregwrapper.component';
import { PwstrengthComponent } from './core/account/pwstrength/pwstrength.component';
import { IndexComponent } from './core/index/index/index.component';
import { ImageuploadComponent } from './core/misc/imageupload/imageupload.component';



// Routes
const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'account/:action', component: LogregwrapperComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    LogregwrapperComponent,
    PwstrengthComponent,
    IndexComponent,
    ImageuploadComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [SocketService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
