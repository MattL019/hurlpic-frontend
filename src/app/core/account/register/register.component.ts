import { Component, OnInit, HostBinding } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidaterService } from '../../../services/account/validater/validater.service';
import { UserService } from '../../../services/account/user/user.service';
import { SocketService } from '../../../services/socket/socket.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [ ValidaterService ]
})
export class RegisterComponent implements OnInit {
  form: FormGroup;
  password: string = "";
  password_input_type: string = "password";

  error: boolean = false; // toggles error box
  success: boolean = false; // toggles success box


  // Set host element width of 100%
  @HostBinding('style.width') width: string = "100%";
  @HostBinding('style.display') display: string = "flex";
  @HostBinding('style.justify-content') justify_content: string = 'center';

  constructor(fb: FormBuilder, private vld: ValidaterService, private us: UserService, private ss: SocketService) {
    this.form = fb.group({
      'username': ['', 
        [
          Validators.required,
          Validators.maxLength(25),
          Validators.minLength(5),
          vld.whitespace,
          vld.specialchars
        ],
        vld.uniqueUsername(this.ss)
      ],
      'email': ['',
        [
          Validators.required,
          Validators.email,
        ],
        vld.uniqueEmail(this.ss)
      ],
      'password': ['',
        [
          Validators.required,
          Validators.minLength(5),
          vld.whitespace
        ]
      ],
      'password_c': ['', 
        [
          vld.match
        ]
      ]
    });
  }

  ngOnInit() {
    setTimeout(() => {
      $('form').removeClass('fadeInUp');
      $('input[type="submit"]').prop('disabled', false);
    },1000);
  }

  onSubmit() {
    
    // Check if form is invalid
    if(this.form.valid) {
      // Form is valid, request response observable
      let responseObs = this.us.register(this.form.value);
      let responseSub = responseObs.subscribe((status: boolean) => {
        if(status) {
          // Registration was successfull
          this.success = true;

        } else {
          // Registration was not successful
          this.error = true;
        }
        responseSub.unsubscribe();
      });
    } else {
      // Play a wiggle animation to signify form is invalid.
      $('form').addClass('wiggle');
      $('input[type="submit"]').prop('disabled', true);
      setTimeout(() => {
        $('form').removeClass('wiggle');
        $('input[type="submit"]').prop('disabled', false);
      },500);
    }
  }

  closeError() {
    // Close error
    this.error = false;
    this.form.reset();
  }
}