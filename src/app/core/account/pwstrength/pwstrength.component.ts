import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-pwstrength',
  templateUrl: './pwstrength.component.html',
  styleUrls: ['./pwstrength.component.scss']
})
export class PwstrengthComponent implements OnInit {
  _password: string;
  strength: number = 0;


  constructor() { }

  ngOnInit() { }

  @Input() 
  set password(password: string) {
    this._password = password;
    
    this.validatePassword(this._password);

    $('.bar-green').css('width', this.strength + "%");
  }

  validatePassword(pw) {
    if(pw === null) {
      this.strength = 0;
      return;
    }

    let strength = 0;

    strength += pw.length * 5;
    strength += pw.replace(/[^0-9]/g, "").length * 20;
    strength += pw.replace(/[a-z]/g, "").length * 20;

    if(/^\d+$/.test(pw)) {
      strength = pw.length * 10;
    }

    this.strength = strength;
  }



}