import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PwstrengthComponent } from './pwstrength.component';

describe('PwstrengthComponent', () => {
  let component: PwstrengthComponent;
  let fixture: ComponentFixture<PwstrengthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PwstrengthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PwstrengthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
