import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-logregwrapper',
  templateUrl: './logregwrapper.component.html',
  styleUrls: ['./logregwrapper.component.scss']
})
export class LogregwrapperComponent implements OnInit {
  action = 0; // 0=register | 1=login

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    // Get parameter
    this.route.paramMap.subscribe(params => {
      if(params.get('action') === "register") this.action = 0; else this.action = 1;
    });
  }

}
