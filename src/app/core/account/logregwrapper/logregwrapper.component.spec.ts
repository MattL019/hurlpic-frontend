import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogregwrapperComponent } from './logregwrapper.component';

describe('LogregwrapperComponent', () => {
  let component: LogregwrapperComponent;
  let fixture: ComponentFixture<LogregwrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogregwrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogregwrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
