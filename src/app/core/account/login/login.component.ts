import { Component, OnInit, HostBinding } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'; 
import { UserService } from '../../../services/account/user/user.service';
import { SocketService } from '../../../services/socket/socket.service';
import { ValidaterService } from '../../../services/account/validater/validater.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [ ValidaterService ]
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  password: string = "";
  password_input_type: string = "password";

  error: boolean = false; // toggles error box
  success: boolean = false; // success box

  // Set host elemtn width of 100%
  @HostBinding('style.width') width: string = "100%";
  @HostBinding('style.display') display: string = "flex";
  @HostBinding('style.justify-content') justify_content: string = 'center';

  constructor(fb: FormBuilder, private vld: ValidaterService, private us: UserService, private ss: SocketService) { 
    this.form = fb.group({
      'username': ['', [ Validators.required ] ],
      'password': ['', [ Validators.required ] ]
    });
  }

  ngOnInit() {
    setTimeout(() => {
        $('form').removeClass('fadeInUp');
        $('input[type="submit"]').prop('disabled', false);
    },1000);
  }

  onSubmit() {

    // Check if form is invalid
    if(this.form.valid) {
      // Form is valid, request response observable

      let responseObs = this.us.login(this.form.value);
      let responseSub = responseObs.subscribe((status: boolean) => {
        if(status) {
          // Login was successfull
          this.success = true;
        } else {
          // Login was unsuccessfull
          this.error = true;
          this.form.controls['username'].setErrors({'invalid': true});
          this.form.controls['password'].setErrors({'invalid': true});
          this.wiggleError();
        }
        responseSub.unsubscribe();
      });



    } else {
      // Form is invalid, play wiggle animation.
      this.wiggleError();
    }
  }

  wiggleError() {
    $('form').addClass('wiggle');
    $('input[type="submit"]').prop('disabled', true);
    setTimeout(() => {
      $('form').removeClass('wiggle');
      $('input[type="submit"]').prop('disabled', false);
    },500);
  }

}
